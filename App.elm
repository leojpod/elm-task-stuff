module Main exposing (..)

import Html exposing (Html, button, div, ul, li, em, span, text, program)
import Html.Events exposing (onClick)
import Date exposing (Date)
import Date.Extra.Config.Config_en_us exposing (config)
import Date.Extra.Format as Format exposing (format, formatUtc, isoMsecOffsetFormat)


-- import Debug
-- MODEL


type alias Model =
    List Item


type alias BasicProps =
    { title : String, description : String }


type alias Assignable a =
    { a | assignee : Maybe String }


type alias HasSubItems a =
    { a | subItems : List Item }


type alias HasDuration a =
    { a | start : Maybe Date, end : Maybe Date }


type alias HasDeadline a =
    { a | deadline : Maybe Date }


type Item
    = Task (HasDuration (Assignable (HasSubItems BasicProps)))
    | SubTask (HasDuration (Assignable BasicProps))
    | Bug (HasDuration (Assignable BasicProps))
    | Epic (HasSubItems (HasDeadline BasicProps))


initialTasks : List Item
initialTasks =
    [ SubTask { title = "find a name", description = "super kaban? something like that?", start = Nothing, end = Nothing, assignee = Nothing }
    , Bug { title = "cannot start the application", description = "it does not work", start = Nothing, end = Nothing, assignee = Nothing }
    , Epic { title = "make a great project", description = "do some great coding... one day", deadline = Nothing, subItems = [] }
    ]


init : ( Model, Cmd Msg )
init =
    ( initialTasks, Cmd.none )



-- MESSAGES


type Msg
    = AllForMe



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick AllForMe ] [ text "assign all to me" ]
        , ul [] (List.map showItem model)
        ]


showItem : Item -> Html Msg
showItem item =
    let
        toStringForAssignable { assignee, title, description } =
            let
                assigneeStr =
                    case assignee of
                        Just a ->
                            a

                        Nothing ->
                            ""
            in
                [ em [] [ text title ]
                , text ":"
                , span [] [ text description ]
                , text ("assigned to " ++ assigneeStr)
                ]
    in
        li []
            (case item of
                Task p ->
                    toStringForAssignable (p)

                SubTask p ->
                    toStringForAssignable (p)

                Bug p ->
                    toStringForAssignable (p)

                Epic { title, description, deadline } ->
                    [ em [] [ text ("Epic " ++ title) ]
                    , text ": "
                    , span [] [ text description ]
                    , em [] [ text "Due on :" ]
                    , text
                        (case deadline of
                            Just deadlineDate ->
                                format config "Y/m/d" deadlineDate

                            Nothing ->
                                "due date not set"
                        )
                    ]
            )



-- UPDATE


assignTo : String -> Item -> Item
assignTo user task =
    let
        updateAssignee taskProperties =
            { taskProperties | assignee = Just user }
    in
        case task of
            Task props ->
                Task (updateAssignee props)

            SubTask props ->
                SubTask (updateAssignee props)

            Bug props ->
                Bug (updateAssignee props)

            Epic props ->
                Epic props



-- i.e. remain unchanged


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AllForMe ->
            ( model |> List.map (\item -> (assignTo "you" item)), Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MAIN


main : Program Never Model Msg
main =
    program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
